def encrypt(pt, alphabet, key):
    ct = ''
    for char in pt:
        ct += alphabet[(alphabet.find(char) + key) % len(alphabet)]
    return ct

def decrypt(ct, alphabet, key):
    pt = '';
    for char in ct:
        pt += alphabet[(alphabet.find(char) - key) % len(alphabet)]
    return pt

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ .'
plain_text = 'Both TeamCity and YouTrack offer free versions/editions that should meet most educational requirements. Just download and use them.'
key = 2
cipher_text = encrypt(plain_text, alphabet, key)
print('Cipher text:')
print(cipher_text)
print('-'*60)
print('Bruteforce')
for k in range(1, len(alphabet)):
    print (decrypt(cipher_text, alphabet, k))
    print ('-'*60)