def get_frequency_sorted(text):
    text_frequency = {}
    for char in text:
        text_frequency[char] = text.count(char) / len(text)
    text_frequency_sorted = list(text_frequency.items())
    text_frequency_sorted.sort(key=lambda item: item[1])
    return text_frequency_sorted

def get_plain_text(ct, cipher_frequency, common_frequency):
    pt = ''
    for char in ct:
        for i in range(0, len(cipher_frequency)):
            if i < len(common_frequency) and cipher_frequency[i][0] == char:
                pt += common_frequency[i][0]
    return pt

letter_frequency = {
    'a': 8.167,
    'b': 1.497,
    'c': 2.782,
    'd': 4.253,
    'e': 12.707,
    'f': 2.228,
    'g': 2.015,
    'h': 6.094,
    'i': 6.966,
    'j': 0.153,
    'k': 0.772,
    'l': 4.025,
    'm': 2.406,
    'n': 6.749,
    'o': 7.507,
    'p': 1.929,
    'q': 0.095,
    'r': 5.987,
    's': 6.327,
    't': 9.056,
    'u': 2.758,
    'v': 0.978,
    'w': 2.361,
    'x': 0.150,
    'y': 1.974,
    'z': 0.074
}


if __name__ == '__main__':
    original_file = open('original.txt')
    original = original_file.read().lower()
    original_file.close()

    cipher_file = open('cipher.txt')
    cipher_text = cipher_file.read().lower()
    cipher_file.close()

    letter_frequency_sorted = list(letter_frequency.items())
    letter_frequency_sorted.sort(key=lambda item: item[1])

    cipher_frequency_sorted = get_frequency_sorted(cipher_text)
    original_frequency_sorted = get_frequency_sorted(original)

    print(original)
    plain_text = get_plain_text(cipher_text, cipher_frequency_sorted, letter_frequency_sorted)
    print('Plain text (wiki frequency):\n' + plain_text)
    plain_text = get_plain_text(cipher_text, cipher_frequency_sorted, original_frequency_sorted)
    print('Plain text (orignal frequency):\n' + plain_text)
