def encrypt(pt, key):
    ct = ''
    for char in pt:
        ct += chr(ord(char) + key)
    return ct

def decrypt(ct, key):
    pt = ''
    for char in ct:
        pt += chr(ord(char) - key)
    return pt

original_file = open('original.txt')
ct = encrypt(original_file.read(), 2)
original_file.close()
cipher_file = open('cipher.txt', 'w')
for i in ct:
    cipher_file.write(i)
cipher_file.close()

print(decrypt(ct, 2))

